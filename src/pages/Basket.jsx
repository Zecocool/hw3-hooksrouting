import ProductList from "../Components/productsList/ProductList";
import { useSelector } from "react-redux";
import BuyForm from "../Components/form/BuyForm";
import styles from "../Style.modules.scss"


export function Basket() {
  const products = useSelector((state) => state.list.products.productsInBasket);
  return products.length ? (
    <div className={styles.Basket}>
      <BuyForm />
      <ProductList page="basket" products={products} />
    </div>
  ) : (
    "Корзина порожня ):"
  );
}


