import ProductList from "../Components/productsList/ProductList";
import { useSelector } from "react-redux";

export function Favorite() {
  const products = useSelector(
    (state) => state.list.products.productsInFavorite
  );

  return products.length ? (
    <ProductList page="favorite" products={products} />
  ) : (
    "There are no goods yet"
  );
}

  