import ProductList from "../Components/productsList/ProductList";

import { useSelector } from "react-redux";


export function Home() {
  const products = useSelector((state) => state.list.products.products);
  return <ProductList page="home" products={products} />;
}
