import { combineReducers } from "redux";
import {listReducer as list } from "./reducers/list";
import { modalReducer as modal} from "./reducers/modal";


export const rootReducer = combineReducers({list, modal});
