import { listTypes } from "../types";

function getAllProducts(products) {
  return {
    type: listTypes.SHOW_PRODUCTS,
    payload: {
      products,
    },
  };
}

export function fetchProductsArray() {
  return async function (dispatch) {
    const data = await fetch("productCollection.json").then((response) => {
      return response.json();
    });
    dispatch(getAllProducts(data));
  };
}

export function addProductToFavorite(product) {
  return {
    type: listTypes.ADD_PRODUCT_TO_FAVORITE,
    payload: {
      product,
    },
  };
}

export function removeProductFromFavorite(article) {
  return {
    type: listTypes.REMOVE_PRODUCT_FROM_FAVORITE,
    payload: {
      article,
    },
  };
}

export function addProductToBasket(product) {
  return {
    type: listTypes.ADD_PRODUCT_TO_BASKET,
    payload: {
      product,
    },
  };
}

export function removeProductFromBasket(article) {
  return {
    type: listTypes.REMOVE_PRODUCT_FROM_BASKET,
    payload: {
      article,
    },
  };
}

export function clearBasket() {
  return {
    type: listTypes.CLEAR_BASKET,
  };
}

