import { listTypes } from "../types";
export function openModal(modalId, submitFunction, data) {
    return {
      type: listTypes.OPEN_MODAL,
      payload: {
        modalId,
        submitFunction,
        data,
      },
    };
  }
  
  export function closeModal() {
    return {
      type: listTypes.CLOSE_MODAL,
    };
  }
  