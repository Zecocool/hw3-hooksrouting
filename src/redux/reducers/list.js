import { listTypes } from "../types";

!localStorage.getItem("productsInFavorite") &&
  localStorage.setItem("productsInFavorite", JSON.stringify([]));

!localStorage.getItem("productsInBasket") &&
  localStorage.setItem("productsInBasket", JSON.stringify([]));

const initialState = {
  products: {
    products: [],
    productsInFavorite: JSON.parse(localStorage.getItem("productsInFavorite")),
    productsInBasket: JSON.parse(localStorage.getItem("productsInBasket")),
  },
};

const getProductsInFavorite = () =>
  JSON.parse(localStorage.getItem("productsInFavorite"));

const getProductsInBasket = () =>
  JSON.parse(localStorage.getItem("productsInBasket"));

export function listReducer(state = initialState, action) {
  switch (action.type) {
    case listTypes.SHOW_PRODUCTS:
      return {
        ...state,
        products: { ...state.products, products: action.payload.products },
      };
    case listTypes.ADD_PRODUCT_TO_FAVORITE:
      localStorage.setItem(
        "productsInFavorite",
        JSON.stringify([...getProductsInFavorite(), action.payload.product])
      );
      return {
        ...state,
        products: {
          ...state.products,
          productsInFavorite: JSON.parse(
            localStorage.getItem("productsInFavorite")
          ),
        },
      };
    case listTypes.REMOVE_PRODUCT_FROM_FAVORITE:
      const productInFavoriteIndex = getProductsInFavorite().findIndex(
        (product) => product.article === action.payload.article
      );
      localStorage.setItem(
        "productsInFavorite",
        JSON.stringify([
          ...getProductsInFavorite().slice(0, productInFavoriteIndex),
          ...getProductsInFavorite().slice(productInFavoriteIndex + 1),
        ])
      );
      return {
        ...state,
        products: {
          ...state.products,
          productsInFavorite: JSON.parse(
            localStorage.getItem("productsInFavorite")
          ),
        },
      };
    case listTypes.ADD_PRODUCT_TO_BASKET:
      localStorage.setItem(
        "productsInBasket",
        JSON.stringify([...getProductsInBasket(), action.payload.product])
      );
      return {
        ...state,
        products: {
          ...state.products,
          productsInBasket: JSON.parse(
            localStorage.getItem("productsInBasket")
          ),
        },
      };
    case listTypes.REMOVE_PRODUCT_FROM_BASKET:
      const productInBasketIndex = getProductsInBasket().findIndex(
        (product) => product.article === action.payload.article
      );
      localStorage.setItem(
        "productsInBasket",
        JSON.stringify([
          ...getProductsInBasket().slice(0, productInBasketIndex),
          ...getProductsInBasket().slice(productInBasketIndex + 1),
        ])
      );
      return {
        ...state,
        products: {
          ...state.products,
          productsInBasket: JSON.parse(
            localStorage.getItem("productsInBasket")
          ),
        },
      };
      case listTypes.CLEAR_BASKET:
      localStorage.setItem("productsInBasket", JSON.stringify([]));
      return {
        ...state,
        ...state.products,
        productsInBasket: JSON.parse(localStorage.getItem("productsInBasket")),
      };

    default:
      return state;
  }
}
