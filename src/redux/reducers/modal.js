import { listTypes } from "../types";

const initialState = {
  modal: {
    visible: false,
    modalId: "default",
    submitFunction: null,
    data: null,
  },
};
export function modalReducer(state = initialState, action) {
  switch (action.type) {
    case listTypes.OPEN_MODAL:
      return {
        ...state,
        modal: {
          visible: true,
          modalId: action.payload.modalId,
          submitFunction: action.payload.submitFunction,
          data: action.payload.data,
        },
      };
    case listTypes.CLOSE_MODAL:
      return {
        ...state,
        modal: {
          visible: false,
          modalId: "default",
          submitFunction: null,
          data: null,
        },
      };
    default:
      return state;
  }
}
