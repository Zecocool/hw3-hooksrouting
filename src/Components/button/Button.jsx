import styleBtn from "./Button.module.scss";
import PropTypes from "prop-types";

function Button({ text, background, onClick }) {
  return (
    <button
      className={styleBtn.Button}
      style={{ background: background }}
      onClick={onClick}
    >
      {text}
    </button>
  );
}

Button.propTypes = {
  background: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
};

export default Button;
