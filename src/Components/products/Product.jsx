import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import Button from "../button/Button";
import styles from "./Product.module.scss";
import favoriteMoney from "../../image/ferstMoney.png";
import favoriteMoneyCommplete from "../../image/secondMoney.png";
import { openModal } from "../../redux/actions/modal";
import {
  addProductToBasket,
  addProductToFavorite,
  removeProductFromFavorite,
  removeProductFromBasket,
} from "../../redux/actions/list";



function Product({ page, product: initialProduct }) {
  const [inBasket, setInBasket] = useState(false);
  const [inFavorite, setInFavorite] = useState(false);
  const [product, setProduct] = useState(initialProduct);
  const dispatch = useDispatch();

  const products = useSelector((state) => state.list.products);

  useEffect(() => {
    setProduct(initialProduct);
    const productInFavorite = products.productsInFavorite.some(
      (item) => item.article === initialProduct?.article
    );
    productInFavorite && setInFavorite(true);

    const productInBasket = products.productsInBasket.some(
      (product) => product.article === initialProduct?.article
    );
    productInBasket && setInBasket(true);
  }, [initialProduct, products.productsInBasket, products.productsInFavorite]);

  const addToFavorite = () => {
    dispatch(addProductToFavorite(product));
    setInFavorite(true);
  };

  const removeFromFavorite = () => {
    dispatch(removeProductFromFavorite(product.article));
    setInFavorite(false);
  };

  const addToBasket = () => {
    dispatch(addProductToBasket(product));
    setInBasket(true);
  };

  const removeFromBasket = () => {
    dispatch(removeProductFromBasket(product.article));
    setInBasket(false);
  };

  const { title, price, imgUrl, color } = product;

  return (
    <div className={page === "basket" ? styles.Basket :styles.Product}>
      <div
        className={styles.Favorite}
        onClick={() => {
          inFavorite ? removeFromFavorite() : addToFavorite();
        }}
      >
        <img src={inFavorite ? favoriteMoney : favoriteMoneyCommplete} alt="" />
      </div>
      <div className={styles.imageWrapper}>
        <img className={styles.ImgProduct} src={imgUrl} alt="" />
      </div>
      <div className={styles.ProductDes}>
        <h2>{title}</h2>
        <h4>Колір: {color}</h4>
        <h3>Ціна: {price} грн</h3>
      </div>
      <Button
        text={(page === "basket" && "Видалити?") ||(inBasket && "Already in the cat") || "Up to the cat"}
        onClick={() => {
          (page === "home" || page === "favorite") && !inBasket
         
            ? dispatch(openModal("addToBasket", addToBasket, product.title))
            : dispatch(
                openModal("removeFromBasket", removeFromBasket, product.title)
              );
        }}
      />
    </div>
  );
}

Product.propTypes = {
  product: PropTypes.object,
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};

export default Product;
