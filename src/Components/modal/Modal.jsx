import Button from "../button/Button";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import modalSettings from "./modalSettings";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../redux/actions/modal";

function Modal() {
  const [settings, setSettings] = useState({});
  const dispatch = useDispatch();

  const { visible, modalId, submitFunction, data } = useSelector(
    ({ modal: { modal } }) => modal
  );

  useEffect(() => {
    const modal = modalSettings.find((item) => item.modalId === modalId);
    setSettings(modal?.settings || {});
  }, [modalId, visible]);

  const { header, closeButton, text, actions } = settings;

  return (
    <div
      className={visible ? styles.ModalWrapper : styles.ModaWrapperlActive}
      onClick={() => dispatch(closeModal())}
    >
      <div
        className={visible ? styles.ModalBody : styles.ModalBodylActive}
        onClick={(e) => e.stopPropagation()}
      >
        <div className={styles.ModalHeader}>
          {header}
          {closeButton && (
            <Button
              text="X"
              background="magenta"
              onClick={() => dispatch(closeModal())}
            />
          )}
        </div>
        <div className={styles.ModalContent}>{text && text(data)}</div>
        <div className={styles.ModalBtnWrapper}>
          {actions &&
            actions.map((item, index) => (
              <Button
                text={item.text}
                onClick={
                  item.type === "submit"
                    ? () => {
                        submitFunction();
                        dispatch(closeModal());
                      }
                    : () => dispatch(closeModal())
                }
                key={Date.now() + index}
              />
            ))}
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  visible: PropTypes.bool,
  modalId: PropTypes.string,
  submit: PropTypes.func,
  closeModal: PropTypes.func,
};

export default Modal;
