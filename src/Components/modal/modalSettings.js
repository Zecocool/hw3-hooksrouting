const modalSettings = [
    
    {
        modalId : "addToBasket",
        settings: {
            header: "Adding a product to the cart",
            closeButton: true,
            text: (data) =>  `You are trying to add a ${data} to the cart. Continue?` ,
            actions:[
                {
                    type:"submit",
                    background: "red",
                    text: "Let's go!"
                },
                {
                    type:"cancel",
                    background:"green",
                    text: "I do not need it"
                }
            ]
        }
    },
    {
        modalId : "removeFromBasket",
        settings: {
            header: "Product removal from the cat",
            closeButton: true,
            text: (data) =>  `Are you sure you want to remove ${data} from your cart?`,
            actions:[
                {
                    type:"submit",
                    background: "red",
                    text: "Let's go!"
                },
                {
                    type:"cancel",
                    background:"green",
                    text: "Leave item in cart"
                }
            ]
        }
    }
];

export default modalSettings;