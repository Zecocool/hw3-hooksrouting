import Product from "../products/Product";
import PropTypes from "prop-types";
import styles from "./ProductList.module.scss";

function ProductList({ page, products }) {
  return (
    <div className={styles.ProductList}>
      {products?.map(({ article, ...product }) => (
        <Product key={article} product={{ article, ...product }} page={page} />
      ))}
    </div>
  );
}

ProductList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object),
  page: PropTypes.string,
};

export default ProductList;
