import { clearBasket } from "../../redux/actions/list";
import *as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { useFormik } from "formik";
import { PatternFormat } from "react-number-format";
import styles from "./Form.module.scss"



const validationSchema = Yup.object({
  firstName: Yup.string()
  .max(12, 'Please enter 12 letters or less')
              .matches(/^[A-Z][a-z]+$/, 'Please enter your name with a capital letter and according to the Latin alphabet.')
              .required('Your name'),
  lastName: Yup.string()
  .max(18, 'Please enter 18 letters or less')
              .matches(/^[A-Z][a-z]+$/, 'Please enter your name with a capital letter and according to the Latin alphabet.')
              .required('Your Last name '),
  age: Yup.number()
  .typeError('Write your age as a number')
              .min(18, 'Your age must be over 18 y.o.')
              .max(100, ' if you are 100 y.o., then you are not with us, but in the book of records)')
              .integer('Write an integer')
              .required('Write age'),
  adress: Yup.string()
  .matches(/^([a-zA-Z\s]+),\s*([a-zA-Z\s]+),\s*([a-zA-Z\s]+),\s*([\d]+)$/, 'You entered the address incorrectly, try this: Kiev, st. Bendery 22')
              .required('Write the address'),
  phoneNumber: Yup.string()
  .matches(/^\(\d{3}\) \d{3}-\d{4}$/, 'Your number is not valid')
              .required('Write down the phone number'),
});


const BuyForm = () => {
  const productsIBasket = useSelector(
    (state) => state.list.products.productsInBasket
  );

  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      age: "",
      adress: "",
      phoneNumber: "",
    },
    validationSchema,
    onSubmit: (values) => {
      const newValues = {
        ...values,
        totalSum: totalSum(),
        products: productsIBasket,
      };
      console.log(newValues);
      dispatch(clearBasket());
    },
  });

  function totalSum() {
    return productsIBasket.reduce(
      (accumulator, product) => accumulator + product.price,
      0
    );
  }

  return (
    <form onSubmit={formik.handleSubmit} className={styles.Form}>
      <div className={styles.Input}>
        <label htmlFor="firstName">Enter a name</label>
        <input
          id="firstName"
          name="firstName"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.firstName}
        />
        {formik.errors.firstName && (
          <span className={styles.Error}>{formik.errors.firstName}</span>
        )}
      </div>
      <div className={styles.Input}>
        <label htmlFor="lastName">Enter your last name</label>
        <input
          id="lastName"
          name="lastName"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.lastName}
        />
        {formik.errors.lastName && (
          <span className={styles.Error}>{formik.errors.lastName}</span>
        )}
      </div>
      <div className={styles.Input}>
        <label htmlFor="age">Enter your age</label>
        <input
          id="age"
          name="age"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.age}
        />
        {formik.errors.age && (
          <span className={styles.Error}>{formik.errors.age}</span>
        )}
      </div>
      <div className={styles.Input}>
        <label htmlFor="adress">Enter your shipping address</label>
        <input
          id="adress"
          name="adress"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.adress}
        />
        {formik.errors.adress && (
          <span className={styles.Error}>{formik.errors.adress}</span>
        )}
      </div>
      <div className={styles.Input}>
        <label htmlFor="phoneNumber">Enter a phone number</label>
        <PatternFormat
          format="(###) ###-####"
          allowEmptyFormatting
          mask="_"
          id="phoneNumber"
          name="phoneNumber"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.phoneNumber}
        />
        {formik.errors.phoneNumber && (
          <span className={styles.Error}>{formik.errors.phoneNumber}</span>
        )}
      </div>
      <span>Total:{totalSum()} грн</span>
      <button type="submit" style={{ cursor: "pointer" }}>
      Buy
      </button>
    </form>
  );
};

export default BuyForm;







